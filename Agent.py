'''
Author: 
    Daniel Pacheco
Description:
    This script makes use of the Keras-rl framework to create an agent capable of learning to play pong 
    using the state of the pixels on screen. Given that Keras-rl does not suport convolutional or pooling 
    layers out the box, all input preprocessing is performed on prior to the input layer.
'''
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Conv1D, MaxPooling1D
from keras.optimizers import Adam

from rl.agents.dqn import DQNAgent
from rl.policy import BoltzmannQPolicy, EpsGreedyQPolicy
from rl.memory import SequentialMemory

from Pong import GameEngine
import gym
import numpy as np

env = GameEngine()
env.reset()
actionSpace = env.action_space.n
num_steps = 10000

#Create the model just like in a Keras network
model = Sequential()
model.add(Flatten(input_shape=(1,) + env.observation_space.shape))
model.add(Dense(128, activation='relu'))
model.add(Dense(32, activation='relu'))
model.add(Dense(actionSpace))
model.add(Activation('linear'))
model.summary()

#Initialize Keras-rl components
memory = SequentialMemory(limit=50000, window_length=1)
policy = EpsGreedyQPolicy()

#Create the DQL Agent, set the model update interval in accordance to the reward function.
dqn = DQNAgent(model=model, nb_actions=actionSpace, memory=memory, nb_steps_warmup=1000, target_model_update=1e-2, policy=policy)

#Select the optimizer and learning rate of your preference.
dqn.compile(Adam(lr=1e-2), metrics=['mse'])

#Start training.
dqn.fit(env, nb_steps=num_steps, visualize=True, verbose=1)

#Save the learned weights for future use.
dqn.save_weights('\\dqn_learnedPong{rf}{ns}_weights.h5f'.format(rf=env.rewardFunction, ns=num_steps), overwrite=True)

#Once the training has finished, test the learned model.
print("Testing Started")
history = dqn.test(env, nb_episodes=5, nb_max_episode_steps=5000, visualize=True)