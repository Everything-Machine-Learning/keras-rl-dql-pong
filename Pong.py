'''
Author: 
    Daniel Pacheco
Description:
    This script generates a Pong-like environment compatible with Keras-rl interfaces.
'''

import math
import pygame
import random
import pygame.surfarray as surfarray
from gym.utils import seeding
import numpy as np
from skimage.measure import block_reduce
 
#Game's colors 
BLACK = (0 ,0, 0)
WHITE = (255, 255, 255)

class Ball(pygame.sprite.Sprite):
 
    def __init__(self):
        super().__init__()
 
        self.image = pygame.Surface([10, 10])
        self.image.fill(WHITE)
        self.rect = self.image.get_rect()
 
        # Get attributes for the height/width of the screen
        self.screenheight = pygame.display.get_surface().get_height()
        self.screenwidth = pygame.display.get_surface().get_width()
 
        # Speed of ball
        self.speed = 0
 
        # coordinates of the ball
        self.x = 0
        self.y = 0
 
        # Direction of ball in degrees
        self.direction = 0
 
        # Height and width of the ball
        self.width = 10
        self.height = 10
 
        # Set the initial ball speed and position
        self.reposition()
 
    def reposition(self):
        self.y = random.randrange(50,100)
        self.x = 100.0
        self.speed=4.0
 
        # Direction of ball
        self.direction = random.randrange(-45,45)
 
        if random.randrange(2) == 0 :
            # Reverse ball direction, let the other guy get it first
            self.direction += 180
            self.x = 100

 
    # This function will bounce the ball off the paddles
    def bounce(self,diff):
        self.direction = (180-self.direction)%360
        self.direction -= diff

        if self.speed < 9:
            # Speed the ball up
            self.speed *= 1.1
 
    # Bounce and move the ball
    def update(self):

        # Sine and Cosine work in degrees, convert to radians
        direction_radians = math.radians(self.direction)
 
        # Change the position (x and y) according to the speed and direction
        self.y += self.speed * math.sin(direction_radians)
        self.x -= self.speed * math.cos(direction_radians)
 
        if self.x < 0:
            self.reposition()
 
        if self.x > 200:
            self.reposition()
 
        # Move the image to where our x and y are
        self.rect.y = self.y
        self.rect.x = self.x
 
        # Do we bounce off the top of the screen?
        if self.y <= 5:
            self.direction = (360-self.direction)%360
 
        # Do we bounce of the bottom of the screen?
        if self.y > self.screenheight-self.height-5:
            self.direction = (360-self.direction)%360


class Player(pygame.sprite.Sprite):
    # Constructor function
    def __init__(self, x_pos):
        # Call the parent's constructor
        super().__init__()
        self.hit = False
        self.height=25
        self.width=8
        self.image = pygame.Surface([self.width, self.height])
        self.image.fill(WHITE)
 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.screenheight = pygame.display.get_surface().get_height()
        self.screenwidth = pygame.display.get_surface().get_width()
 
        self.rect.y = 0
        self.rect.x = x_pos
 
    # Update the player, actions = [0:down, 1:stay, 2:up]
    def update(self, action):

        if action == 0:
            vert_axis_pos = -1
        if action == 1:
            vert_axis_pos = 0
        if action == 2:
            vert_axis_pos = 1
 
        # Move x according to the axis. We multiply by 15 to speed up the movement.
        self.rect.y=int(self.rect.y+vert_axis_pos*10)
 
        # Make sure we don't push the player paddle off the right side of the screen
        if self.rect.y > self.screenheight - self.height:
            self.rect.y = self.screenheight - self.height
        elif self.rect.y < 0:
            self.rect.y = 0
    
    #puts the paddle on the same heigh as ball
    def followBall(self, ballYCoord):
        self.rect.y=int(ballYCoord - 10)
                
        # Make sure we don't push the player paddle off the right side of the screen
        if self.rect.y > self.screenheight - self.height:
            self.rect.y = self.screenheight - self.height
        elif self.rect.y < 0:
            self.rect.y = 0


#This class is the main object of the environment, to run this environment instantiate this class and call runContinuous() or step()
class GameEngine():

    def __init__(self):
        self.stepCount = 0
        self.score1 = 0
        self.score2 = 0
        self.reward = 0
        self.action_space = Discrete(3)
        self.observation_space = Box(low=0, high=1, shape=(50, 75, 1), dtype=np.float32)
        self.functionsDict = {
                                "YOverlap": self.YOverlap, 
                                "YDistParab": self.YDistParabola, 
                                "inverseEucledian": self.inverseEucledian,
                                "isHit": self.isHit
                            }

        self.rewardFunction = "YOverlap"
    
        # Call this function so the Pygame library can initialize itself
        pygame.init()
    
        # Create an 200X150 sized screen
        self.screen = pygame.display.set_mode([200, 150])
    
        # Set the title of the window
        pygame.display.set_caption('DQL Pong')
    
        # Enable this to make the mouse disappear when over our window
        pygame.mouse.set_visible(0)
    
        # This is a font we use to draw text on the screen (size 18)
        self.font = pygame.font.Font(None, 18)
    
        # Create a surface we can draw on
        self.background = pygame.Surface(self.screen.get_size())
    
        # Create the ball
        self.ball = Ball()

        # Create a group of 1 ball (used in checking collisions)
        self.balls = pygame.sprite.Group()
        self.balls.add(self.ball)

        # Create the player paddle object
        self.player1 = Player(185)
        self.player2 = Player(6)
    
        self.movingsprites = pygame.sprite.Group()
        self.movingsprites.add(self.player1)
        self.movingsprites.add(self.player2)
        self.movingsprites.add(self.ball)
    
        self.clock = pygame.time.Clock()
        self.done = False
        self.exit_program = False

    def getRewardFunction(self):
        return self.rewardFunction

    def setRewardFunction(self, functionName):
        if functionName in self.functionsDict.keys:
            self.rewardFunction = functionName
        else:
            print("No function with name '%s' was found", functionName)

    def runContinuous(self):
        while not self.exit_program:
        
            # Clear the screen
            self.screen.fill(BLACK)
        
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.exit_program = True
        
            # Stop the game if there is an imbalance of 3 points
            if abs(self.score1 - self.score2) > 20:
                print("Done")
                self.done = True
        
            if not self.done:
                # Update the player and ball positions
                self.player1.update(random.randint(-1,1))
                self.player2.followBall(self.ball.y)
                self.ball.update()
        
            # If we are done, print game over
            if self.done:
                text = self.font.render("Game Over", 1, (200, 200, 200))
                textpos = text.get_rect(centerx=background.get_width()/2)
                textpos.top = 25
                self.screen.blit(text, textpos)
        
            # See if the ball hits the player paddle
            if pygame.sprite.spritecollide(self.player1, self.balls, False):
                # The 'diff' lets you try to bounce the ball left or right depending where on the paddle you hit it
                diff = (self.player1.rect.x + self.player1.width/2) - (self.ball.rect.x+self.ball.width/2)
        
                # Set the ball's x position in case we hit the ball on the edge of the paddle
                self.ball.x = 170
                self.ball.bounce(diff)
                self.score1 += 1
        
            # See if the ball hits the player paddle
            if pygame.sprite.spritecollide(self.player2, self.balls, False):
                # The 'diff' lets you try to bounce the ball left or right depending where on the paddle you hit it
                diff = (self.player2.rect.x + self.player2.height/2) - (self.ball.rect.x+self.ball.height/2)
        
                # Set the ball's x position in case we hit the ball on the edge of the paddle
                self.ball.x = 20
                self.ball.bounce(diff)
                self.score2 += 1
        
            # Print the score
            scoreprint = "DQL: "+str(self.score1)
            text = self.font.render(scoreprint, 1, WHITE)
            textpos = (125, 0)
            self.screen.blit(text, textpos)
        
            scoreprint = "Rigged: "+str(self.score2)
            text = self.font.render(scoreprint, 1, WHITE)
            textpos = (50, 0)
            self.screen.blit(text, textpos)
        
            # Draw Everything
            self.movingsprites.draw(self.screen)
        
            # Update the screen
            pygame.display.flip()

            #returns an array of pixels corresponding to the state of the display.
            #array = surfarray.array3d(pygame.display.get_surface())

            self.clock.tick(50)
            

    
    #Transform a screen observation into a 2D array.
    def preprocessObservation(self, observation):
        observation = np.asarray(observation)
        observation = observation[:,0 :,0]
        observation[observation != 0] = 1
        observation = observation.astype(np.float32)
        observation = block_reduce(observation, (2,1), np.max)
        observation = block_reduce(observation, (2,2), np.max)
        observation = observation.reshape(observation.shape[0], observation.shape[1], 1)
        return observation


    def YOverlap(self, player, ball, threshold=20, reward=10):
        yDist = abs((player.rect.y + 10) - ball.y)
        if yDist < threshold:
            return reward
        else:
            return 0

    def YDistParabola(self, player, ball):
        yDist = abs((player.rect.y + 10) - ball.y)
        if yDist < 50:
            return (-0.00333*(yDist**2)) + 10
        else:
            return 0

    def inverseEucledian(self, player, ball):
        if abs(ball.direction) > 90 and abs(ball.direction) < 270:
            yDist = (player.rect.y + 10) - ball.y
            xDist = player.rect.x - ball.x
            EucDist = math.sqrt(yDist**2 + xDist**2)
            eps = 0.0001
            return 10000/(EucDist + eps)
        else:
            return 0

    def isHit(self, player, ball):
        if player.hit:
            player.hit = False
            return 10
        else:
            return 0


    def step(self, playerAction):

        self.reward = 0
        # Clear the screen
        self.screen.fill(BLACK)
    
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
    
            # if abs(self.score1 - self.score2) > 100:
            #     self.done = True
    
        if not self.done:
            # Update the player and ball positions

            #Deep Q Learning player
            self.player1.update(playerAction)

            #Rigged player
            self.player2.followBall(self.ball.y)

            #update ball
            self.ball.update()
    
        # If we are done, print game over
        if self.done:
            self.ball.reposition()
            self.score1 = 0
            self.score2 = 0
            self.reward = 0
            print("DQL: ", self.score1, "Rigged: ", self.score2)
    
        # See if the ball hits the player paddle
        if pygame.sprite.spritecollide(self.player1, self.balls, False):

            self.player1.hit = True

            #bounce the ball left or right depending where on the paddle you hit it
            diff = (self.player1.rect.x + self.player1.width/2) - (self.ball.rect.x+self.ball.width/2)

            # Set the ball's x position in case we hit the ball on the edge of the paddle
            self.ball.x = 170
            self.ball.bounce(diff)
            self.score1 += 1
    
        # See if the ball hits the player paddle
        if pygame.sprite.spritecollide(self.player2, self.balls, False):
            #bounce the ball left or right depending where on the paddle you hit it
            diff = (self.player2.rect.x + self.player2.height/2) - (self.ball.rect.x+self.ball.height/2)
    
            # Set the ball's x position in case we hit the ball on the edge of the paddle
            self.ball.x = 15
            self.ball.bounce(diff)
            self.score2 += 1
    
        # Print score
        scoreprint = "DQL: "+str(self.score1)
        text = self.font.render(scoreprint, 1, WHITE)
        textpos = (125, 0)
        self.screen.blit(text, textpos)
    
        scoreprint = "Rigged: "+str(self.score2)
        text = self.font.render(scoreprint, 1, WHITE)
        textpos = (25, 0)
        self.screen.blit(text, textpos)
    
        # Draw Everything
        self.movingsprites.draw(self.screen)
    
        # Update the screen
        pygame.display.flip()

        #returns an array of pixels corresponding to the state of the display. we will pass only one color channel.
        pygame.display.get_surface().lock()
        observation = self.preprocessObservation(np.asarray(surfarray.array3d(pygame.display.get_surface())))
        pygame.display.get_surface().unlock() 

        # #calculate reward
        self.reward = self.functionsDict[self.rewardFunction](self.player1,self.ball)

        #this line dictates the speed the game runs at, if commented out, max speed
        self.clock.tick(50)

        return observation, self.reward, self.done, {"ale.lives": self.score1}

    def reset(self):
        print("DQL: ", self.score1, "Rigged: ", self.score2)
        self.ball.reposition()
        self.score1 = 0
        self.score2 = 0
        self.reward = 0
        self.done = False
        observation, _r, _d, _i = self.step(2)
        return observation

    def render(self, mode='human'):
        pass

    def sample(self):
        return np.random.randint(0, high=3)


####################################################################################################################
#                               'OpenAI gym' interfaces necessary for Keras-rl integration                         #
####################################################################################################################

class Space(object):
    """Defines the observation and action spaces, so you can write generic
    code that applies to any Env. For example, you can choose a random
    action.
    """
    def __init__(self, shape=None, dtype=None):
        import numpy as np  # takes about 300-400ms to import, so we load lazily
        self.shape = None if shape is None else tuple(shape)
        self.dtype = None if dtype is None else np.dtype(dtype)
        self.np_random = None
        self.seed()

    def sample(self):
        """Randomly sample an element of this space. Can be 
        uniform or non-uniform sampling based on boundedness of space."""
        raise NotImplementedError

    def seed(self, seed=None):
        """Seed the PRNG of this space. """
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def contains(self, x):
        """
        Return boolean specifying if x is a valid
        member of this space
        """
        raise NotImplementedError

    def __contains__(self, x):
        return self.contains(x)

    def to_jsonable(self, sample_n):
        """Convert a batch of samples from this space to a JSONable data type."""
        # By default, assume identity is JSONable
        return sample_n

    def from_jsonable(self, sample_n):
        """Convert a JSONable data type to a batch of samples from this space."""
        # By default, assume identity is JSONable
        return sample_n


class Discrete(Space):
    r"""A discrete space in :math:`\{ 0, 1, \\dots, n-1 \}`. 
    Example::
        >>> Discrete(2)
    """
    def __init__(self, n):
        assert n >= 0
        self.n = n
        super(Discrete, self).__init__((), np.int64)

    def sample(self):
        return self.np_random.randint(self.n)

    def contains(self, x):
        if isinstance(x, int):
            as_int = x
        elif isinstance(x, (np.generic, np.ndarray)) and (x.dtype.kind in np.typecodes['AllInteger'] and x.shape == ()):
            as_int = int(x)
        else:
            return False
        return as_int >= 0 and as_int < self.n

    def __repr__(self):
        return "Discrete(%d)" % self.n

    def __eq__(self, other):
        return isinstance(other, Discrete) and self.n == other.n


class Box(Space):
    """
    A (possibly unbounded) box in R^n. Specifically, a Box represents the
    Cartesian product of n closed intervals. Each interval has the form of one
    of [a, b], (-oo, b], [a, oo), or (-oo, oo).
    
    There are two common use cases:
    
    * Identical bound for each dimension::
        >>> Box(low=-1.0, high=2.0, shape=(3, 4), dtype=np.float32)
        Box(3, 4)
        
    * Independent bound for each dimension::
        >>> Box(low=np.array([-1.0, -2.0]), high=np.array([2.0, 4.0]), dtype=np.float32)
        Box(2,)
    """
    def __init__(self, low, high, shape=None, dtype=np.float32):
        assert dtype is not None, 'dtype must be explicitly provided. '
        self.dtype = np.dtype(dtype)

        if shape is None:
            assert low.shape == high.shape, 'box dimension mismatch. '
            self.shape = low.shape
            self.low = low
            self.high = high
        else:
            assert np.isscalar(low) and np.isscalar(high), 'box requires scalar bounds. '
            self.shape = tuple(shape)
            self.low = np.full(self.shape, low)
            self.high = np.full(self.shape, high)

        self.low = self.low.astype(self.dtype)
        self.high = self.high.astype(self.dtype)

        # Boolean arrays which indicate the interval type for each coordinate
        self.bounded_below = -np.inf < self.low
        self.bounded_above = np.inf > self.high

        super(Box, self).__init__(self.shape, self.dtype)

    def is_bounded(self, manner="both"):
        below = np.all(self.bounded_below)
        above = np.all(self.bounded_above)
        if manner == "both":
            return below and above
        elif manner == "below":
            return below
        elif manner == "above":
            return above
        else:
            raise ValueError("manner is not in {'below', 'above', 'both'}")

    def sample(self):
        """
        Generates a single random sample inside of the Box. 
        In creating a sample of the box, each coordinate is sampled according to
        the form of the interval:
        
        * [a, b] : uniform distribution 
        * [a, oo) : shifted exponential distribution
        * (-oo, b] : shifted negative exponential distribution
        * (-oo, oo) : normal distribution
        """
        high = self.high if self.dtype.kind == 'f' \
                else self.high.astype('int64') + 1
        sample = np.empty(self.shape)

        # Masking arrays which classify the coordinates according to interval
        # type
        unbounded   = ~self.bounded_below & ~self.bounded_above
        upp_bounded = ~self.bounded_below &  self.bounded_above
        low_bounded =  self.bounded_below & ~self.bounded_above
        bounded     =  self.bounded_below &  self.bounded_above
        

        # Vectorized sampling by interval type
        sample[unbounded] = self.np_random.normal(
                size=unbounded[unbounded].shape)

        sample[low_bounded] = self.np_random.exponential(
            size=low_bounded[low_bounded].shape) + self.low[low_bounded]
        
        sample[upp_bounded] = -self.np_random.exponential(
            size=upp_bounded[upp_bounded].shape) - self.high[upp_bounded]
        
        sample[bounded] = self.np_random.uniform(low=self.low[bounded], 
                                            high=high[bounded],
                                            size=bounded[bounded].shape)
        if self.dtype.kind == 'i':
            sample = np.floor(sample)

        return sample.astype(self.dtype)
        
    def contains(self, x):
        if isinstance(x, list):
            x = np.array(x)  # Promote list to array for contains check
        return x.shape == self.shape and np.all(x >= self.low) and np.all(x <= self.high)

    def to_jsonable(self, sample_n):
        return np.array(sample_n).tolist()

    def from_jsonable(self, sample_n):
        return [np.asarray(sample) for sample in sample_n]

    def __repr__(self):
        return "Box" + str(self.shape)

    def __eq__(self, other):
        return isinstance(other, Box) and (self.shape == other.shape) and np.allclose(self.low, other.low) and np.allclose(self.high, other.high)


class Dict(Space):
    """
    A dictionary of simpler spaces.
    Example usage:
    self.observation_space = spaces.Dict({"position": spaces.Discrete(2), "velocity": spaces.Discrete(3)})
    Example usage [nested]:
    self.nested_observation_space = spaces.Dict({
        'sensors':  spaces.Dict({
            'position': spaces.Box(low=-100, high=100, shape=(3,)),
            'velocity': spaces.Box(low=-1, high=1, shape=(3,)),
            'front_cam': spaces.Tuple((
                spaces.Box(low=0, high=1, shape=(10, 10, 3)),
                spaces.Box(low=0, high=1, shape=(10, 10, 3))
            )),
            'rear_cam': spaces.Box(low=0, high=1, shape=(10, 10, 3)),
        }),
        'ext_controller': spaces.MultiDiscrete((5, 2, 2)),
        'inner_state':spaces.Dict({
            'charge': spaces.Discrete(100),
            'system_checks': spaces.MultiBinary(10),
            'job_status': spaces.Dict({
                'task': spaces.Discrete(5),
                'progress': spaces.Box(low=0, high=100, shape=()),
            })
        })
    })
    """
    def __init__(self, spaces=None, **spaces_kwargs):
        assert (spaces is None) or (not spaces_kwargs), 'Use either Dict(spaces=dict(...)) or Dict(foo=x, bar=z)'
        if spaces is None:
            spaces = spaces_kwargs
        if isinstance(spaces, dict) and not isinstance(spaces, OrderedDict):
            spaces = OrderedDict(sorted(list(spaces.items())))
        if isinstance(spaces, list):
            spaces = OrderedDict(spaces)
        self.spaces = spaces
        for space in spaces.values():
            assert isinstance(space, Space), 'Values of the dict should be instances of gym.Space'
        super(Dict, self).__init__(None, None) # None for shape and dtype, since it'll require special handling

    def seed(self, seed=None):
        [space.seed(seed) for space in self.spaces.values()]

    def sample(self):
        return OrderedDict([(k, space.sample()) for k, space in self.spaces.items()])

    def contains(self, x):
        if not isinstance(x, dict) or len(x) != len(self.spaces):
            return False
        for k, space in self.spaces.items():
            if k not in x:
                return False
            if not space.contains(x[k]):
                return False
        return True

    def __getitem__(self, key):
        return self.spaces[key]

    def __repr__(self):
        return "Dict(" + ", ". join([str(k) + ":" + str(s) for k, s in self.spaces.items()]) + ")"

    def to_jsonable(self, sample_n):
        # serialize as dict-repr of vectors
        return {key: space.to_jsonable([sample[key] for sample in sample_n]) \
                for key, space in self.spaces.items()}

    def from_jsonable(self, sample_n):
        dict_of_list = {}
        for key, space in self.spaces.items():
            dict_of_list[key] = space.from_jsonable(sample_n[key])
        ret = []
        for i, _ in enumerate(dict_of_list[key]):
            entry = {}
            for key, value in dict_of_list.items():
                entry[key] = value[i]
            ret.append(entry)
        return ret

    def __eq__(self, other):
        return isinstance(other, Dict) and self.spaces == other.spaces

#Run game with agent taking random actions
# engine = GameEngine()
# trials = 0
# ratios = []
# d = 0
# while not d:
#     observation, reward, done, info = engine.step(random.randint(0,2))
#     d = done