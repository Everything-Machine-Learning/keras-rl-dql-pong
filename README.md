# Simple Reinforcement Learning Pong Agent

**This program requires [Keras-rl](https://github.com/keras-rl/keras-rl) and [Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)**

A simple Keras-rl based Deep Q Learning agent that learns to play Pong. The short script `Agent.py` creates and trains a reinforcement learning agent using the Pong-like environment instantiated in `Pong.py`. The script makes use of the [PyGame](https://www.pygame.org/docs/) framework in order to provide a customizable reward function system.


![](media/YOverlap10000_test.mp4)
_Training and testing video capture_


## Reward Functions

The `Pong.py` script includes several different reward functions illustrated below:

![YOverlap](media/YOverlap.png "YOverlap") 
**YOverlap**: The agent receives a reward everytime the paddle is at the same height as the ball (the green area).

![YParabDist](media/YParabDist.png)
**YParabDist**: Similar to YOverlap but instead of a discrete reward, the agent receives a continuous valued reward within a distance. 

![InverseEucledian](media/InverseEucledian.png) 
**InverseEucledian**: The agent receives a reward inversely proportional to the Eucledian distance from the paddle to the ball.

![isHit](media/isHit.png)
**isHit**: The agent only receives rewards when the ball hits the paddle.

_Each reward function will affect the effectiveness and required time of the training process. May be necessary to adjust the policy strategy (ie. Boltzmann, EpsilonGreedy, etc.)_

## How to Run

###### 1. Install Keras-rl

To install Keras-rl, follow the [link](https://github.com/keras-rl/keras-rl) and download the repository files to a local directory, then open a command prompt and navigate to the downloaded directory:

```
$ cd keras-rl-master
$ python setup.py
```
###### 2. Setup conda environment

To install Conda, click the [link](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) and folow installation instructions.

Once Conda has been installed, create the python environment described in `environment-pong.yml` To do this, open a command prompt and navigate to the directory containing `environment-pong.yml` then execute:

```
$ conda env create -f environment.yml
```

###### 3. Run the agent 

Once Keras-rl and Conda are installed, activate the conda environment:

```
$ conda activate Everything-ML
```

Then run start the training by executing:

```
$ python Agent.py
```
